//
//  AppDelegate.h
//  Hello World
//
//  Created by Aaron Jiji on 1/27/16.
//  Copyright © 2016 Aaron Jiji. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

